create table if not exists logs
(
	id serial not null primary key,
	qname varchar(253) not null,
	qtype integer not null,
	ip inet not null,
	create_time timestamp default now() not null
);