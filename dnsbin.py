#!/usr/bin/env python3

import asyncio
import dnslib
import aiohttp
from asyncpg import create_pool
from os import environ

DB_HOST = environ.get('DB_HOST')
DB_USERNAME = environ.get('DB_USERNAME')
DB_PASSWORD = environ.get('DB_PASSWORD')
DB_NAME = environ.get('DB_NAME')
MIN_WORKER = int(environ.get('MIN_WORKER', 1))
MAX_WORKER = int(environ.get('MAX_WORKER', 10))
DOMAIN = environ.get('DOMAIN')
TG_TOKEN = environ.get('TG_TOKEN')
TG_CHATID = environ.get('TG_CHATID')


class DNSBinProtocol:
    def __init__(self):
        asyncio.ensure_future(self.worker_init())

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        asyncio.ensure_future(self.worker(data, addr))

    async def worker_init(self):
        self.pool = await create_pool(host=DB_HOST, database=DB_NAME,
                                      user=DB_USERNAME, password=DB_PASSWORD,
                                      min_size=MIN_WORKER, max_size=MAX_WORKER)

    async def worker(self, data, addr):
        request = dnslib.DNSRecord.parse(data)
        qname = request.q.qname
        qtype = request.q.qtype
        if not qname.matchSuffix(DOMAIN):
            return False
        response = dnslib.DNSRecord(
            dnslib.DNSHeader(
                id=request.header.id,
                qr=1,
                aa=1),
            q=request.q)
        response.add_answer(
            dnslib.RR(
                qname,
                qtype,
                ttl=3600,
                rdata=dnslib.A('127.0.0.1')))
        self.transport.sendto(response.pack(), addr)
        async with self.pool.acquire() as conn:
            await conn.execute('INSERT INTO logs (qname,qtype,ip) VALUES ($1,$2,$3)', str(qname), qtype, addr[0])
        if TG_TOKEN is not None:
            async with aiohttp.ClientSession() as session:
                await session.get(
                    'https://api.telegram.org/bot{token}/sendmessage?chat_id={chat_id}&text={text}'.format(
                        token=TG_TOKEN, chat_id=TG_CHATID, text=str(qname)))
        return True


loop = asyncio.get_event_loop()
listen = loop.create_datagram_endpoint(
    DNSBinProtocol, local_addr=('0.0.0.0', 53))
transport, protocol = loop.run_until_complete(listen)

try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

transport.close()
loop.close()
