FROM python:3.7-alpine
LABEL maintainer="imlonghao <dockerfile@esd.cc>"
WORKDIR /app
COPY dnsbin.py ./
RUN apk add --no-cache --virtual build-dependencies g++ && \
    pip install --no-cache-dir dnslib asyncpg aiohttp && \
    apk del build-dependencies
ENTRYPOINT [ "/app/dnsbin.py" ]